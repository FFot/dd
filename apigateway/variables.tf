variable "aws_region" {
  type    = string
  default = "eu-west-2"
}

variable "aws_profile" {
  type    = string
  default = "academy2"
}

variable "prefix" {
  type    = string
  default = "dd"
}

variable "tags" {
  default = {
    "Name"      = "DDAPIGW"
    "Project"   = "ALAcademy2020"
    "Team"      = "Daemon Demons"
    "Info"      = "Test API Gateway with API Key"
    "Owner"     = "Silviana Horga"
    "StartDate" = "20200601"
    "EndDate"   = "20200731"
  }
}
