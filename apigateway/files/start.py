import boto3
region = 'us-west-2'
ec2 = boto3.client('ec2', region_name=region)
filters = [
{
    'Name': 'tag:Name',
    'Values': ['test-DD-Master', 'test-DD-Worker']
}]
reservations = ec2.describe_instances(Filters = filters)

def lambda_handler(event, context):
    ec2.start_instances(InstanceIds = reservations.Instances.InstanceIds)
    print('started your instances: ' + str(instances))
